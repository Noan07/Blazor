﻿using AnimeList.Models;

namespace AnimeList.Services
{
    public interface IDataService
    {
        Task Add(AnimeModel model);

        Task<int> Count();

        Task<List<Anime>> List(int currentPage, int pageSize);

        Task<Anime> GetById(int id);

        Task Update(int id, AnimeModel model);
        Task Delete(int id);
        Task<List<Anime>> All();
        Task GetByIdAndUpdate(int id, string grp);
    }
}
