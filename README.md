[![Build Status](https://codefirst.iut.uca.fr/api/badges/noan.randon/Blazor/status.svg)](https://codefirst.iut.uca.fr/noan.randon/Blazor)

# Projet Application Blazor

## Lancement de l'Application

Notre projet se situe sur la branche master.

Afin de pouvoir lancer la solution, il faut tout d'abord effectuer un clique droit sur la solution, puis aller sur "Propriété" et cocher "Plusieurs projets au démarrage" , et mettre toutes les actions à "Démarrer", enfin cliquez sur appliquer.

À partir de là vous pouvez éxécuter la solution!

## Présentation de notre projet

Notre application web a pour but de référencer essentiellement des animes, mais aussi de partager des animes via la liste d'anime.
Les utilisateurs ont accès à une tiers list.

## Contributeurs

RANDON Noan 
SAOULA Zakariya

## Bareme

- Mise en place d'une page de visualisation des données avec pagination (2 points)
- Page d'ajout d'un élement avec validation (2 point)
- Page d'édition d'un élement avec validation (2 point)
- Supression d'un élement avec une confirmation (2 point)
- Composant complexe (5 point)
- Use API (Get / Insert / Update / Delete) (3 point)
- Utilisation IOC & DI (4 point)
- Localisation & Globalisation (au moins deux langues) (1 point)
- Utilisation de la configuration (1 point)
- Logs (2 point)
- Propreté du code (Vous pouvez vous servir de sonarqube) (2 point)
- IHM (Design global, placement des boutons, ...) (2 point)
- Emplacement du code (Pas de code dans les vues) (2 point)
- Le Readme (2 points)
- Description du fonctionnement de la solution client (illustrutration au niveau du code) (6 points)
- Merge request (2 points)