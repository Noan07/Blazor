﻿using System.Text.Json.Serialization;

namespace Minecraft.Crafting.Api.Models
{
    public class Anime
    {
        public Anime()
        {
            Status = new List<string>();
            Genres = new List<string>();
            Sources = new List<string>();
        }
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("titre")]
        public string Titre { get; set; }
        [JsonPropertyName("producteurs")]
        public string Producteurs { get; set; }
        [JsonPropertyName("status")]
        public List<string> Status { get; set; }
        [JsonPropertyName("genres")]
        public List<string> Genres { get; set; }
        [JsonPropertyName("sources")]
        public List<string> Sources { get; set; }
        [JsonPropertyName("groupe")]
        public string Groupe { get; set; }
        [JsonPropertyName("episodes")]
        public int Episodes { get; set; }
        [JsonPropertyName("dateParution")]
        public DateTime DateParution { get; set; }
        [JsonPropertyName("imageBase64")]

        public string ImageBase64 { get; set; }
    }
}
