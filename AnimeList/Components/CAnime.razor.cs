﻿using AnimeList.Models;
using AnimeList.Services;
using Microsoft.AspNetCore.Components;

namespace AnimeList.Components
{
    public partial class CAnime
    {


        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IDataService DataService { get; set; }

        [Parameter]
        public int Id { get; set; }

        [Parameter]
        public string Image { get; set;  }

        [Parameter]
        public string Titre { get; set; }


        private void NavigateToDetail()
        {

            NavigationManager.NavigateTo("detail/"+Id);
        }


    }
}
