﻿namespace AnimeList.Models
{
    public class Anime
    {
        public int Id { get; set; }
        public string Titre { get; set; }
        public string Producteurs { get; set; }
        public List<string> Status { get; set; }
        public List<string> Genres { get; set; }
        public List<string> Sources { get; set; }
        //public string Image { get; set; }

        public string Groupe { get; set; }
        public int Episodes { get; set; }
        public DateTime DateParution { get; set; }

        public string ImageBase64 { get; set; }
    }
}
