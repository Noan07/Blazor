﻿using AnimeList.Models;
using AnimeList.Services;
using Blazorise;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace AnimeList.Pages
{
    public partial class TiersList
    {
        [Inject]
        public IStringLocalizer<TiersList> Localizer { get; set; }

        [Inject]
        public IDataService DataService { get; set; }

        private List<Anime> animes;

        private int totalAnime;


        protected override async Task OnInitializedAsync()
        {
            //animes = await DataService.All();
            totalAnime = await DataService.Count();
            animes = await DataService.List(1, totalAnime);
        }

        private async Task GetAnime(int id, string grp)
        {
             await DataService.GetByIdAndUpdate(id,grp);
        }

        private  Task ItemDropped(DraggableDroppedEventArgs<Anime> dropItem)
        {

            _ = GetAnime(dropItem.Item.Id, dropItem.DropZoneName);
            dropItem.Item.Groupe = dropItem.DropZoneName;
            return Task.CompletedTask;
        }
    }
}
