﻿using AnimeList.Models;

namespace AnimeList.Factories
{
    public static class ItemFactory
    {
        public static AnimeModel ToModel(Anime anime, byte[] imageContent)
        {
            return new AnimeModel
            {
                Id = anime.Id,
                Titre = anime.Titre,
                Producteurs = anime.Producteurs,
                Genres = anime.Genres,
                Status = anime.Status,
                Sources = anime.Sources,
                Episodes = anime.Episodes,
                DateParution = anime.DateParution,
                ImageContent = imageContent,
                ImageBase64 = string.IsNullOrWhiteSpace(anime.ImageBase64) ? Convert.ToBase64String(imageContent) : anime.ImageBase64
            };
        }

        public static Anime Create(AnimeModel model)
        {
            return new Anime
            {
                Id = model.Id,
                Titre = model.Titre,
                Producteurs = model.Producteurs,
                Genres = model.Genres,
                Status = model.Status,
                Sources = model.Sources,
                Groupe = "Liste",
                Episodes = model.Episodes,
                DateParution = model.DateParution,
                ImageBase64 = Convert.ToBase64String(model.ImageContent)
            };
        }

        public static void Update(Anime anime, AnimeModel model)
        {
            anime.Id = model.Id;
            anime.Titre = model.Titre;
            anime.Producteurs = model.Producteurs;
            anime.Genres = model.Genres;
            anime.Status = model.Status;
            anime.Sources = model.Sources;
            anime.Episodes = model.Episodes;
            anime.DateParution = model.DateParution;
            anime.ImageBase64 = Convert.ToBase64String(model.ImageContent);
        }
    }
}
