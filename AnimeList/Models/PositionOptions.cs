﻿namespace AnimeList.Models
{
    public class PositionOptions
    {
        public const string Position = "Position";

        public string Title { get; set; }
        public List<string> Names { get; set; }

        public string School { get; set; }
        public string Study { get; set; }
    }
}
