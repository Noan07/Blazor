var searchData=
[
  ['add_0',['Add',['../class_anime_list_1_1_services_1_1_data_api_service.html#a4e613769aed185b2823c50be3cdfbc46',1,'AnimeList.Services.DataApiService.Add()'],['../class_anime_list_1_1_services_1_1_data_local_service.html#acfcbb4b5529829e5ffed66342d711559',1,'AnimeList.Services.DataLocalService.Add()'],['../interface_anime_list_1_1_services_1_1_i_data_service.html#a1c4fff8137980d0eb8af454df046f7b6',1,'AnimeList.Services.IDataService.Add()']]],
  ['addblazoredlocalstorage_1',['AddBlazoredLocalStorage',['../_program_8cs.html#a123e39cc7c9ef6e668dea1e793557dd0',1,'Program.cs']]],
  ['addblazoredmodal_2',['AddBlazoredModal',['../_program_8cs.html#a0459f19714637a57cced2c56d1d3b3bd',1,'Program.cs']]],
  ['addblazorise_3',['AddBlazorise',['../_program_8cs.html#a70630b91d80f40f085c85c3c303af9ce',1,'Program.cs']]],
  ['addconfiguration_4',['AddConfiguration',['../_program_8cs.html#ad375b1daa5a2c47534ba87361004584e',1,'Program.cs']]],
  ['addcontrollers_5',['AddControllers',['../_program_8cs.html#abc3694a0e15618f2499e5a540264f833',1,'Program.cs']]],
  ['addhttpclient_6',['AddHttpClient',['../_program_8cs.html#a32d7bc2afdb2ab1b8cd24332feeb0aec',1,'Program.cs']]],
  ['addlocalization_7',['AddLocalization',['../_program_8cs.html#a79229590c0926b8dcc317e5541a79437',1,'Program.cs']]],
  ['addrazorpages_8',['AddRazorPages',['../_program_8cs.html#a50ac3cc59e3f8d9e5c1a2734bd14b7f5',1,'Program.cs']]],
  ['addscoped_3c_20idataservice_2c_20dataapiservice_20_3e_9',['AddScoped&lt; IDataService, DataApiService &gt;',['../_program_8cs.html#a4a75f0a786a33a34548d7518f45e7aa4',1,'Program.cs']]],
  ['addserversideblazor_10',['AddServerSideBlazor',['../_program_8cs.html#abda56024f5b832bc28b5198cc50fbf0f',1,'Program.cs']]],
  ['all_11',['All',['../class_anime_list_1_1_services_1_1_data_api_service.html#ad8ddd6906a6a156352dbe362315c9e4f',1,'AnimeList.Services.DataApiService.All()'],['../class_anime_list_1_1_services_1_1_data_local_service.html#a9a05974383c76d7c8bff4d73caf58327',1,'AnimeList.Services.DataLocalService.All()'],['../interface_anime_list_1_1_services_1_1_i_data_service.html#a45fd61567280547c68e715feeefeabf9',1,'AnimeList.Services.IDataService.All()']]]
];
