var searchData=
[
  ['update_0',['Update',['../class_anime_list_1_1_services_1_1_data_api_service.html#a32133142f93fa6e5a3c6f57e6ffcf4a4',1,'AnimeList.Services.DataApiService.Update()'],['../class_anime_list_1_1_services_1_1_data_local_service.html#a9e6b11ea4fe9a3c420a3e24ffe7408fd',1,'AnimeList.Services.DataLocalService.Update()'],['../interface_anime_list_1_1_services_1_1_i_data_service.html#aa61908e79f1d666af8647fdc0398520d',1,'AnimeList.Services.IDataService.Update()']]],
  ['useendpoints_1',['UseEndpoints',['../_program_8cs.html#a5ede1aa3e4362df1c1dece947e6f696a',1,'Program.cs']]],
  ['usehttpsredirection_2',['UseHttpsRedirection',['../_program_8cs.html#aa4d447fc3129a3aa301d736b8bd04ae9',1,'Program.cs']]],
  ['userouting_3',['UseRouting',['../_program_8cs.html#a94c810d266751293a2d511a720a5625f',1,'Program.cs']]],
  ['usestaticfiles_4',['UseStaticFiles',['../_program_8cs.html#a906a3ce545279a7a73941f1d7b64d7cf',1,'Program.cs']]]
];
