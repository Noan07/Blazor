﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace AnimeList.Shared
{
    public partial class NavMenu
    {
        [Inject]
        public IStringLocalizer<NavMenu> Localizer { get; set; }
    }
}
