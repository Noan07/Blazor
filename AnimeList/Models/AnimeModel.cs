﻿using System.ComponentModel.DataAnnotations;

namespace AnimeList.Models
{
    public class AnimeModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Le nom de l'animé ne doit pas dépasser 50 caractères.")]
        public string Titre { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Le producteur ne doit pas dépasser 50 caractères.")]
        public string Producteurs { get; set; }
        public List<string> Genres { get; set; }

        public List<string> Status { get; set; }

        public List<string> Sources { get; set; }

        [Required]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Vous devez accepter les conditions.")]
        public bool AcceptCondition { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date de parution")]
        public DateTime DateParution { get; set; }

        [Required]
        [Range(1, 2000)]
        public int Episodes { get; set; }

        public string ImageBase64 { get; set; }

        [Required(ErrorMessage = "L'image de l'item est obligatoire !")]
        public byte[] ImageContent { get; set; }
    }
}
