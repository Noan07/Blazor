﻿using AnimeList.Factories;
using AnimeList.Models;
using AnimeList.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Localization;

namespace AnimeList.Pages
{
    public partial class Edit
    {
        [Inject]
        public IStringLocalizer<Add> Localizer { get; set; }
        [Parameter]
        public int Id { get; set; }

        private List<string> Genres = new List<string>() { "Comedie", "Surnaturel", "Sport", "Combat", "Romance" };

        private List<string> Sources = new List<string>() { "Manga", "LightNovel", "Anime", "Film" };

        private List<string> Status = new List<string>() { "En cours", "Terminé", "En attente", "Inconnue" };

        private AnimeModel animeModel = new()
        {
            Sources = new List<string>(),
            Genres = new List<string>(),
            Status = new List<string>()
        };

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IWebHostEnvironment WebHostEnvironment { get; set; }

        protected override async Task OnInitializedAsync()
        {
            var item = await DataService.GetById(Id);

            var fileContent = await File.ReadAllBytesAsync($"{WebHostEnvironment.WebRootPath}/images/default.png");
            animeModel = ItemFactory.ToModel(item, fileContent);
        }

        private async void HandleValidSubmit()
        {
            await DataService.Update(Id, animeModel);

            NavigationManager.NavigateTo("list");
        }

        private async Task LoadImage(InputFileChangeEventArgs e)
        {
            using (var memoryStream = new MemoryStream())
            {
                await e.File.OpenReadStream().CopyToAsync(memoryStream);
                animeModel.ImageContent = memoryStream.ToArray();
            }
        }

        private void OnGenresChange(string genre, object checkedValue)
        {
            if ((bool)checkedValue)
            {
                if (!animeModel.Genres.Contains(genre))
                {
                    animeModel.Genres.Add(genre);
                }

                return;
            }

            if (animeModel.Genres.Contains(genre))
            {
                animeModel.Genres.Remove(genre);
            }
        }

        private void OnSourcesChange(string source, object checkedValue)
        {
            if ((bool)checkedValue)
            {
                if (!animeModel.Sources.Contains(source))
                {
                    animeModel.Sources.Add(source);
                }

                return;
            }

            if (animeModel.Sources.Contains(source))
            {
                animeModel.Sources.Remove(source);
            }
        }

        private void OnStatusChange(string status, object checkedValue)
        {
            if ((bool)checkedValue)
            {
                if (!animeModel.Status.Contains(status))
                {
                    animeModel.Status.Add(status);
                }

                return;
            }

            if (animeModel.Status.Contains(status))
            {
                animeModel.Status.Remove(status);
            }
        }
    }
}
