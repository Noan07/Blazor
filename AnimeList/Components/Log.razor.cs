﻿

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace AnimeList.Components
{
    public partial class Log
    {
        [Inject]
        public IStringLocalizer<Config> Localizer { get; set; }

        [Inject]
        public ILogger<Log> Logger { get; set; }

        private void CreateLogs()
        {
            var logLevels = Enum.GetValues(typeof(LogLevel)).Cast<LogLevel>();

            foreach (var logLevel in logLevels.Where(l => l != LogLevel.None))
            {
                Logger.Log(logLevel, $"Log message for the level: {logLevel}");
            }
        }
    }
}
