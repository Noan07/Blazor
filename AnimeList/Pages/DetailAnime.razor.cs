﻿using AnimeList.Models;
using AnimeList.Services;
using Blazorise;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace AnimeList.Pages
{
    public partial class DetailAnime
    {
        [Inject]
        public IStringLocalizer<Add> Localizer { get; set; }

        private Anime anime;

        [Parameter]
        public int Id { get; set; }


        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IDataService DataService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            anime = await DataService.GetById(Id);
        }

        void Cancel()
        {
            NavigationManager.NavigateTo("/");
        }
    }
}
