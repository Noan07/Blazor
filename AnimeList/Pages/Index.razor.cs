﻿using AnimeList.Models;
using AnimeList.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace AnimeList.Pages
{
    public partial class Index
    {
        [Inject]
        public IStringLocalizer<Index> Localizer { get; set; }

        private AnimeIndex[]? animes;

        private List<Anime>? animespre = new();
        private int currentPage = 1;
        private const int pageSize = 4;

        [Inject]
        public HttpClient Http { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        [Inject]
        public IDataService DataService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            animes = await Http.GetFromJsonAsync<AnimeIndex[]>($"{NavigationManager.BaseUri}data-car.json");
            animespre = await DataService.All();
        }

        private void Augmenter()
        {
            if (currentPage < animespre.Count() / pageSize)
                currentPage++;
        }

        private void Diminuer()
        {
            if (currentPage > 1)
                currentPage--;
        }

        private List<Anime>? animes2 => animespre.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
    }
}
