﻿using AnimeList.Factories;
using AnimeList.Models;

namespace AnimeList.Services
{
    public class DataApiService : IDataService
    {
        private readonly HttpClient _http;

        public DataApiService(
            HttpClient http)
        {
            _http = http;
        }

        public async Task Add(AnimeModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            // Save the data
            await _http.PostAsJsonAsync("https://localhost:7234/api/Crafting/", item);
        }

        public async Task<int> Count()
        {
            return await _http.GetFromJsonAsync<int>("https://localhost:7234/api/Crafting/count");
        }

        public async Task<List<Anime>> List(int currentPage, int pageSize)
        {
            return await _http.GetFromJsonAsync<List<Anime>>($"https://localhost:7234/api/Crafting/?currentPage={currentPage}&pageSize={pageSize}");
        }

        public async Task<List<Anime>> All()
        {
            return await _http.GetFromJsonAsync<List<Anime>>($"https://localhost:7234/api/Crafting/all");
        }

        public async Task<Anime> GetById(int id)
        {
            return await _http.GetFromJsonAsync<Anime>($"https://localhost:7234/api/Crafting/{id}");
        }

        public async Task Update(int id, AnimeModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            await _http.PutAsJsonAsync($"https://localhost:7234/api/Crafting/{id}", item);
        }


        public async Task GetByIdAndUpdate(int id, string grp)
        {
            await _http.PutAsJsonAsync($"https://localhost:7234/api/Crafting/IdUpdate/{id}", grp);
        }

        public async Task Delete(int id)
        {
            await _http.DeleteAsync($"https://localhost:7234/api/Crafting/{id}");
        }
    }
}
