﻿namespace AnimeList.Models
{
    public class AnimeTiersList
    {
        public string Groupe { get; set; }
        public string Titre { get; set; }
        public string ImageBase64 { get; set; }
    }
}
