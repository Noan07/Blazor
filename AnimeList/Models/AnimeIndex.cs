﻿namespace AnimeList.Models
{
    public class AnimeIndex
    {
        public int Id { get; set; }
        public string Titre { get; set; }

        public string Image { get; set; }
    }
}
