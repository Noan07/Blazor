﻿using AnimeList.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;

namespace AnimeList.Components
{
    public partial class Config
    {
        [Inject]
        public IStringLocalizer<Config> Localizer { get; set; }

        [Inject]
        public IConfiguration Configuration { get; set; }

        [Inject]
        public IOptions<PositionOptions> OptionsPositionOptions { get; set; }

        private PositionOptions positionOptions;

        protected override void OnInitialized()
        {
            base.OnInitialized();

            positionOptions = OptionsPositionOptions.Value;
        }
    }
}