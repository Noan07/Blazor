﻿using AnimeList.Models;
using AnimeList.Services;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.Extensions.Localization;

namespace AnimeList.Pages
{ 

    public partial class Add
    {
        [Inject]
        public IStringLocalizer<Add> Localizer { get; set; }
        /// <summary>
        /// The default enchant categories.
        /// </summary>
        private List<string> Genres = new List<string>() { "Comedie", "Surnaturel", "Sport", "Combat", "Romance" };

        /// <summary>
        /// The default repair with.
        /// </summary>
        private List<string> Sources = new List<string>() { "Manga", "LightNovel", "Anime", "Film" };

        private List<string> Status = new List<string>() { "En cours", "Terminé", "En attente", "Inconnue" };

        /// <summary>
        /// The current item model
        /// </summary>
        private AnimeModel animeModel = new()
        {
            Sources = new List<string>(),
            Genres = new List<string>(),
            Status = new List<string>()
        };

        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }

        private async void HandleValidSubmit()
        {
            await DataService.Add(animeModel);

            NavigationManager.NavigateTo("list");
        }

        private async Task LoadImage(InputFileChangeEventArgs e)
        {
            // Set the content of the image to the model
            using (var memoryStream = new MemoryStream())
            {
                await e.File.OpenReadStream().CopyToAsync(memoryStream);
                animeModel.ImageContent = memoryStream.ToArray();
            }
        }

        private void OnGenresChange(string genre, object checkedValue)
        {
            if ((bool)checkedValue)
            {
                if (!animeModel.Genres.Contains(genre))
                {
                    animeModel.Genres.Add(genre);
                }

                return;
            }

            if (animeModel.Genres.Contains(genre))
            {
                animeModel.Genres.Remove(genre);
            }
        }

        private void OnSourcesChange(string source, object checkedValue)
        {
            if ((bool)checkedValue)
            {
                if (!animeModel.Sources.Contains(source))
                {
                    animeModel.Sources.Add(source);
                }

                return;
            }

            if (animeModel.Sources.Contains(source))
            {
                animeModel.Sources.Remove(source);
            }
        }

        private void OnStatusChange(string status, object checkedValue)
        {
            if ((bool)checkedValue)
            {
                if (!animeModel.Status.Contains(status))
                {
                    animeModel.Status.Add(status);
                }

                return;
            }

            if (animeModel.Status.Contains(status))
            {
                animeModel.Status.Remove(status);
            }
        }
    }
}
